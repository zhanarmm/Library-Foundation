import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import data.LibraryData;
import enums.LibraryDataType;
import enums.OperationTypes;
import services.OperationExecutorService;

public class ClientRunner {
	
    public static void main(String[] args) throws IOException, InterruptedException {
    	
    	OperationExecutorService.manager.add(new LibraryData(LibraryDataType.BOOK,"����� ������", "�������", "1985", 55, "������������", "�����", "����� �������"));
    	OperationExecutorService.manager.add(new LibraryData(LibraryDataType.JOURNAL, " ", "�������1", "01.1986", 56, "������������", "������ ������", " "));
    	OperationExecutorService.manager.add(new LibraryData(LibraryDataType.BROCHURE," " ,"�������2", "11.1987", 0, "������������", "������� �������� �������", " "));
      
        for (OperationTypes operation : OperationTypes.values()) {
            System.out.println(operation.getId() + ") " + operation.getName());
        }
        while(true){
           vyzov();
        }
    

    }
public static void vyzov() throws IOException, InterruptedException{
	 System.out.println(" ");
    System.out.print("�������� ��������: ");
    try  
    {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    int chosenOperation = Integer.parseInt(reader.readLine());

    switch (chosenOperation) {
        case 1:
            // �������� ������������������ ������� � �����
            OperationExecutorService.viewRegisteredEditions(OperationExecutorService.manager.getDatas()); break;
        case 2:
            // ���������� ������ ������� � ����
            OperationExecutorService.addingNewEdition(reader); break;
        case 3:
            // �������� ���������� ���������� ������� 
        	 OperationExecutorService.viewInformationOfTheSelectEdition(OperationExecutorService.manager.getDatas(), reader); break;
        case 4:
            // �������� ���������� �������
        OperationExecutorService.removeSelectedEdition(OperationExecutorService.manager.getDatas(), reader); break;
            
        case 5:
            // ����� �� �������
            System.exit(1); break;

        default:
            System.out.println("�������� �������� �� ������"); break;
    }}
    catch (NumberFormatException e)
    {
       
        System.out.println("������������ ������ �����");
    
  }
}


}