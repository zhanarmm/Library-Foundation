package data;

import enums.LibraryDataType;

public class LibraryData {
    private int id;        // ������������� ������
    private LibraryDataType type;      // ��� - �������, ������ ��� �����
    private String name;   // ������������ �����, ������� ��� �������
    //private Date date;     // (���/�����/����) ������������
    private String year;
    private int pageCount; // ���������� �������
    private String isbn;   // ������������
    private String zhanr; //����
    private String avtor; //�����
    private String content; //����������
    public LibraryData() {
    }

    public LibraryData(LibraryDataType type, String avtor,String name, String year, int pageCount, String isbn,String zhanr, String content) {
        this.type = type;
        this.avtor = avtor;
        this.name = name;
        this.year = year;
        this.pageCount = pageCount;
        this.isbn = isbn;
        this.zhanr = zhanr;
        this.content = content;
    }



    // getters and setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LibraryDataType getType() {
        return type;
    }

    public void setType(LibraryDataType type) {
        this.type = type;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getAvtor() {
        return avtor;
    }

    public void setAvtor(String avtor) {
        this.avtor = avtor;
    }
    public String getZhanr() {
        return zhanr;
    }

    public void setZhanr(String zhanr) {
        this.zhanr = zhanr;
    }
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.zhanr = content;
    }
    
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }


    @Override
    public String toString() {
        return "LibraryData{" +
                "type=" + type +
                ", name='" + name + '\'' +
                ", year=" + year +
                ", pageCount=" + pageCount +
                '}';
    }
}
