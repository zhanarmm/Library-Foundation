package enums;

public enum LibraryDataType {
    BOOK, JOURNAL, BROCHURE
}
