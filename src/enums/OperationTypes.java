package enums;

public enum OperationTypes {

    VIEW_REGISTERED_EDITIONS_IN_THE_FUND(1, "�������� ������������������ ������� � �����"),
    ADDING_NEW_EDITION(2, "���������� ������ ������� � ����"),
    VIEW_INFORMATION_OF_THE_SELECTED_EDITION(3, "�������� ���������� ���������� �������"),
    REMOVE_SELECTED_EDITION(4, "�������� ���������� �������"),
    EXIT(5, "�����");

    private final int id;
    private final String name;

    OperationTypes(final int id, final String name) {
        this.id = id;
        this.name = name;
    }


    // getters
    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
}
