package services;

import data.LibraryData;
import enums.LibraryDataType;
import manager.LibraryDataManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

public class OperationExecutorService {

    public static LibraryDataManager manager = new LibraryDataManager();

    /**
     * �������� ������������������ ������� � ����
     */
    public static void viewRegisteredEditions(List<LibraryData> datas) {
        datas.forEach(s -> System.out.println (datas.indexOf(s)+1 +", "+ s.getType()+", "+ s.getAvtor()+", "+ s.getName()+", "+ s.getYear()+", "+ s.getPageCount()+", "+ s.getIsbn()+", "+ s.getZhanr()+", "+ s.getContent()));
   
    }

    /**
     * ���������� ����� ������ --------------------- OperationTypes.ADDING_NEW_EDITION
     * @throws IOException
     */
    public static void addingNewEdition(BufferedReader reader) throws IOException {
        LibraryData libraryData = new LibraryData();
        System.out.println("��������� ��� ���� ��� �������� �������");
        System.out.print("��� - �������, ������ ��� �����: " +
                "1) �����   2) ������   3) �������");
        int type = Integer.parseInt(reader.readLine());
        int pageCount = 0;
        String avtor = null, name= null, isbn= null,zhanr = null, content= null, year = null;
        switch (type) {
            case 1:
                libraryData.setType(LibraryDataType.BOOK);
                System.out.print("����� �����: ");
                avtor = reader.readLine();
                System.out.print("������������ �����, ������� ��� �������: ");
                 name = reader.readLine();
                 System.out.print("��� ������������, ���� ��� ����� ��� �� ����� � ��� ������������ � ������ ������� ��� �������: ");
                 year = reader.readLine();
                 System.out.print("������������: ");
                 isbn = reader.readLine();
                System.out.print("���������� �������: ");
                pageCount = Integer.parseInt(reader.readLine());
                System.out.print("���� �����: ");
                zhanr = reader.readLine();
                System.out.print("������� ����������: ");
                content = reader.readLine();
                break;
            case 2:
            
                libraryData.setType(LibraryDataType.JOURNAL);
                avtor = " ";
            	System.out.print("������������ �����, ������� ��� �������: ");
                name = reader.readLine();
                System.out.print("��� ������������, ���� ��� ����� ��� �� ����� � ��� ������������ � ������ ������� ��� �������: ");
                year = reader.readLine();
                System.out.print("������������: ");
                isbn = reader.readLine();
                System.out.print("���������� �������: ");
                pageCount = Integer.parseInt(reader.readLine());
                System.out.print("������ ������: ");
                zhanr = reader.readLine();
                content ="";
                break;
            case 3:
                libraryData.setType(LibraryDataType.BROCHURE);
                avtor = "";
                System.out.print("������������ �����, ������� ��� �������: ");
                name = reader.readLine();
                System.out.print("��� ������������, ���� ��� ����� ��� �� ����� � ��� ������������ � ������ ������� ��� �������: ");
                year = reader.readLine();
                System.out.print("������������: ");
                isbn = reader.readLine();
               pageCount = 0;
               zhanr = "";
               content ="";
                break;
            default:
                System.out.println("�������� ��� �� ������"); break;
        }

     
        LibraryData data = new LibraryData(libraryData.getType(),avtor, name, year, pageCount, isbn,zhanr, content);
        manager.add(data);
        System.out.println("������� ���������");}
       
    
    
    /**
     * �������� ��������
     */
    public static void viewInformationOfTheSelectEdition(List<LibraryData> datas, BufferedReader reader) throws IOException {
    	try{
    		System.out.print("������� ������ �������, ������� ������ ����������: ");
    	 int selectId = Integer.parseInt(reader.readLine()) - 1;
    	 System.out.println("���������� � �������");
         System.out.println (selectId+1 +"  "+ datas.get(selectId).getType()+"  "+ datas.get(selectId).getAvtor()+"  "+ datas.get(selectId).getName()+"  "+ datas.get(selectId).getYear()+"  "+ datas.get(selectId).getPageCount()+"  "+ datas.get(selectId).getIsbn()+"  "+ datas.get(selectId).getZhanr()+"  "+ datas.get(selectId).getContent());

    	}
        catch (IndexOutOfBoundsException e)
        {
        	 System.out.println("� ���� ��� ������ �������");
    	
    }}
    /**
     * �������� ��������
     */
    public static void removeSelectedEdition(List<LibraryData> datas, BufferedReader reader) throws IOException {
    	try{
    	System.out.print("������� ������ �������, ������� ������ �������: ");
    	 int removeId = Integer.parseInt(reader.readLine()) - 1;
    	 datas.remove(removeId);
    	 System.out.println("������� �������");
	}
    catch (IndexOutOfBoundsException e)
    {
    	 System.out.println("� ���� ��� ������ �������");
	
}
    }
}
